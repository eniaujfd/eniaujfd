import os
from datetime import datetime

# Folder to move files to
destination_folder = "C:\\Users\\Fennecus\\Documents\\bash_in\\out"

# Folder to move files from (use the folder where this script is run)
source_folder = os.path.dirname(os.path.abspath(__file__))

# We want to rename and move every file in the folder, lets do that with a for statement
for filename in os.listdir(source_folder):

    # We only want to to the operation for pdfs lets skip all files that are not pdf
    extension = filename.split('.')[-1]
    if extension != 'pdf':
        # File is not pdf, lets stop here, and try again with the next file
        continue

    # Get creation timestamp
    original_file = os.path.join(source_folder, filename)
    time_unix = os.path.getctime(original_file)
    time_readable = datetime.utcfromtimestamp(time_unix).strftime('%Y-%m-%d %H%M%S')

    # Lets verify that the file doesn't already have the correct timestamp
    if filename.startswith(time_readable):
        # File has the correct prefix, avoid duplicating it
        continue

    # Create the new name by joining destination path with timestamp and original filename
    new_file = os.path.join(destination_folder, time_readable) + ' ' + filename

    # Rename the file
    os.rename(original_file, new_file)