import os
from datetime import datetime

# get current path
pwd = os.path.dirname(os.path.abspath(__file__))

for filename in os.listdir("."):
    extension = filename.split('.')[-1]
    if extension == 'pdf':
        # Get creation timestamp
        original_file = os.path.join(pwd, filename)
        time_unix = os.path.getctime(original_file)
        time_readable = datetime.utcfromtimestamp(time_unix).strftime('%Y-%m-%d %H%M%S')

        # Lets verify that the file doesn't already have the correct timestamp
        if filename.startswith(time_readable):
            # File has the correct prefix, avoid duplicating it
            continue

        # Create the new name
        new_file = os.path.join(pwd, time_readable) + ' ' + filename

        # Rename the file
        os.rename(original_file, new_file)